# "Le petit plus"
Modele et patron - Version 3 - par FannyBarme - licence CCO.

Proposé en 3 tailles, L, M et S pour s’adapter à la morphologie de
chacun.    
2 variantes :
 - Variante 1 : Masque 2 couches avec une ouverture en bas
permettant d’insérer une troisième couche, amovible. (Cette
variante est très utile pour ceux qui veulent insérer une couche
jetable entre les deux couches tissus).
 - Variante 2 : Masque 3 couches cousues ensemble

## Note
par RemRem, le 31/03/2020 :
 - les fichiers pdf seront touchés prochainement pour être allégés et corrigés.
